package lib;

public class Stack<T> {
  private final LinkedList<T> stack = new LinkedList<>(false, true);
  public T top = null;
  public Integer maxSize = null;

  public Stack(Integer maxSize) {
    this.maxSize = maxSize;
  }

  public Stack() {}

  public void push(T value) {
    int currentSize = this.stack.size();
    if (currentSize < this.maxSize) {
      this.stack.insert(value);
      this.top = this.stack.head.getValue();
    } else {
      System.out.println("Stack overflow");
    }
  }

  public T pop() {
    if (stack.head != null) {
      T valueToPop = stack.head.getValue();
      this.stack.remove(valueToPop);
      this.top = this.stack.head.getValue();
      return valueToPop;
    }
    System.out.println("Stack underflow");
    return null;
  }

  public boolean isEmpty() {
    return this.stack.head == null;
  }
}
