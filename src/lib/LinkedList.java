package lib;

public class LinkedList<T> {
  public Node<T> head = null;
  public Node<T> tail = null;
  private final boolean isCircular;
  private final boolean isSingly;

  public static class Node<T> {
    private T value;
    private Node<T> previous = null;
    private Node<T> next = null;

    public Node(T value) {
      this.value = value;
    }

    public T getValue() {
      return value;
    }

    public void setValue(T value) {
      this.value = value;
    }

    public Node<T> getPrevious() {
      return previous;
    }

    public void setPrevious(Node<T> previous) {
      this.previous = previous;
    }

    public Node<T> getNext() {
      return next;
    }

    public void setNext(Node<T> next) {
      this.next = next;
    }

    @Override
    public String toString() {
      T previous = getPrevious() != null ? getPrevious().getValue() : null;
      T next = getNext() != null ? getNext().getValue() : null;
      return "Node{" + "previous=" + previous + ", value=" + value + ", next=" + next + '}';
    }
  }

  public LinkedList(boolean isCircular, boolean isSingly) {
    this.isCircular = isCircular;
    this.isSingly = isSingly;
  }

  public LinkedList() {
    this.isCircular = true;
    this.isSingly = false;
  }

  private void setCircularNodes() {
    if (this.isCircular) {
      this.tail.setNext(this.head);
      if (!this.isSingly) {
        this.head.setPrevious(this.tail);
      }
    }
  }

  public void insert(T value) {
    Node<T> node = new Node<>(value);
    if (this.head == null) {
      this.head = node;
      this.tail = node;
    } else {
      if (this.tail == this.head) {
        if (!this.isSingly) {
          this.tail.setPrevious(node);
        }
        this.head = node;
        this.head.setNext(this.tail);
      } else {
        node.setNext(this.head);
        if (!this.isSingly) {
          this.head.setPrevious(node);
        }
        this.head = node;
      }
    }
    this.setCircularNodes();
  }

  public void append(T value) {
    Node<T> node = new Node<>(value);
    if (this.tail == null) {
      this.tail = node;
      this.head = node;
    } else {
      if (this.head == this.tail) {
        this.head.setNext(node);
        this.tail = node;
        if (!this.isSingly) {
          this.tail.setPrevious(this.head);
        }
      } else {
        if (!this.isSingly) {
          node.setPrevious(this.tail);
        }
        this.tail.setNext(node);
        this.tail = node;
      }
    }
    this.setCircularNodes();
  }

  public void update(T currentValue, T newValue) {
    Node<T> current = this.head;
    Node<T> previous = this.head;
    while (current != null) {
      if (current.getValue() == currentValue) {
        if (current == this.head) {
          this.head.setValue(newValue);
        } else {
          current.setValue(newValue);
          if (!this.isSingly) {
            current.getNext().setPrevious(current);
          }
          previous.setNext(current);
        }
        break;
      }
      previous = current;
      current = current.getNext();
    }
  }

  public void remove(T value) {
    Node<T> current = this.head;
    Node<T> previous = this.head;
    while (current != null) {
      if (current.getValue() == value) {
        if (current == this.head) {
          this.head = current.getNext();
          if (this.head != null && !this.isSingly) {
            this.head.setPrevious(null);
          }
        } else if (current == this.tail) {
          this.tail = this.tail.getPrevious();
          this.head.setPrevious(this.tail);
        } else {
          previous.setNext(current.getNext());
          if (current.getNext() != null && !this.isSingly) {
            current.getNext().setPrevious(previous);
          }
        }
        this.setCircularNodes();
        break;
      }
      previous = current;
      current = current.getNext();
    }
  }

  public int size() {
    int count = 0;
    Node<T> current = this.head;
    while (this.head != null) {
      count++;
      if (current.getNext() == this.head || current.getNext() == null) {
        break;
      } else {
        current = current.getNext();
      }
    }
    return count;
  }

  public void display() {
    Node<T> current = this.head;
    boolean check = false;
    while (!check) {
      System.out.println(current.toString());
      if (this.isCircular) {
        if (current.getNext() == this.head) {
          check = true;
        }
      } else {
        if (current.getNext() == null) {
          check = true;
        }
      }
      current = current.getNext();
    }
  }
}
